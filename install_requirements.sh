#!/bin/bash

# Update host
yum update -y && yum upgrade -y
USER_NAME=vagrant
USER_HOME_DIR=/home/${USER_NAME}
USER_SHELL_RC=$USER_HOME_DIR/.zshrc
USER_BIN_DIR=${USER_HOME_DIR}/bin
TEMP_DIR=${USER_HOME_DIR}/temp
TERRAFORM_VERSION=0.13.5

[ -e ${USER_BIN_DIR}] || mkdir -p $USER_BIN_DIR
[ -e ${TEMP_DIR}] || mkdir -p $TEMP_DIR

sudo - $USER_NAME && cd $USER_HOME_DIR

# Insstall needed dependencies
yum install -y gcc make build-essential epel-release net-tools
yum install -y git tmux htop tree vim wget curl zip unzip

yum install -y yum-utils gcc gcc-c++ make wget
yum groupinstall -y  development
yum install -y python-devel openldap-devel openssl-devel zlib zlib-devel libffi-devel
yum install -y bzip2-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel expat-devel

# Install zsh, oh-my-zsh and apply
yum install -y zsh
#       https://github.com/ohmyzsh/ohmyzsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
#       Fonts https://github.com/powerline/fonts
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts && ./install.sh
cd .. && rm -rf fonts
#       zsh-syntax-highlighting plugin
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
#       zsh-autosuggestions plugin
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
#       setup tmux conf
echo "set -g mouse on
set-option -g default-shell /bin/zsh
" >> $USER_HOME_DIR/.tmux.conf
#       install additional themes
#           https://github.com/Powerlevel9k/powerlevel9k
#           https://github.com/denysdovhan/spaceship-prompt
#       Install Spaceship https://dev-props.com/notes/spaceship/
git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt"
ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"
#       setup .zshrc
echo '
export ZSH="${HOME}/.oh-my-zsh"
ZSH_THEME="dstufft"
SPACESHIP_GIT_BRANCH_COLOR="yellow"

plugins=(
    git python django npm npx aws vault terraform ansible 
    docker docker-compose kubectl zsh-syntax-highlighting zsh-autosuggestions 
)

source $ZSH/oh-my-zsh.sh

# Shortcat for the pretty git log. Can be extended with the commit count parameter (last -10, last -35)
alias last="git log --graph --all --oneline --decorate "

export PATH="${HOME}/bin:$PATH"
' > $USER_SHELL_RC

# Install ansible awscli sshuttle
yum install -y ansible awscli sshuttle

# Install Docker and Docker-compose
# https://docs.docker.com/engine/install/centos/
yum remove docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-engine

yum install -y yum-utils
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce docker-ce-cli containerd.io
systemctl start docker
systemctl enable docker
usermod -aG docker $USER_NAME
curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/sbin/docker-compose
chmod +x /usr/local/sbin/docker-compose

cd $TEMP_DIR
# Install Terraform
wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip
mv terrafo* /usr/local/sbin

# Install Vault client
wget https://releases.hashicorp.com/vault/1.0.3/vault_1.0.3_linux_amd64.zip
unzip vault_1.0.3_linux_amd64.zip && rm vault_1.0.3_linux_amd64.zip
mv vaul* /usr/local/sbin

# Install PyEnv
git clone https://github.com/pyenv/pyenv.git $USER_HOME_DIR/.pyenv
echo '
## pyenv configs
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
eval "$(pyenv init -)"
fi' >> $USER_SHELL_RC


# Install MongoDB
echo '[mongodb-org-4.4]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/4.4/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.4.asc' > /etc/yum.repos.d/mongodb-org-4.4.repo
yum install -y mongodb-org
yum install -y mongodb-org-4.4.1 mongodb-org-server-4.4.1 mongodb-org-shell-4.4.1 mongodb-org-mongos-4.4.1 mongodb-org-tools-4.4.1

mkdir -p /var/lib/mongo
mkdir -p /var/log/mongodb
chown -R mongod:mongod /var/lib/mongo
chown -R mongod:mongod /var/log/mongodb

# Git console upgrade
echo "
# Show a current active git branch in the shell prompt
export PS1='\t \[\033[01;32m\]\u\[\033[01;34m\] \w\[\033[01;33m\]$(__git_ps1)\[\033[01;34m\] \$\[\033[00m\] '

# Shortcat for the pretty git log. Can be extended with the commit count parameter (last -10, last -35)
alias last='git log --graph --all --oneline --decorate '" >> $USER_SHELL_RC

# Last prepare
chmod +x "$USER_BIN_DIR/*" > /dev/null
chown -R $USER_NAME:$USER_NAME $USER_HOME_DIR `ls $USER_HOME_DIR` `ls $USER_BIN_DIR` > /dev/null