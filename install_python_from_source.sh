#!/bin/bash

PYTHON_VERSION=$1
USER_NAME=vagrant
USER_HOME_DIR=/home/${USER_NAME}
PYTHON_BASE_DIR=${USER_HOME_DIR}/.python/${PYTHON_VERSION}
TEMP_DIR=${USER_HOME_DIR}/temp

echo "======== Install Python $PYTHON_VERSION ========"

yum install -y yum-utils gcc gcc-c++ make wget
yum groupinstall -y  development
yum install -y python-devel openldap-devel openssl-devel zlib zlib-devel libffi-devel
yum install -y bzip2-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel expat-devel
yum-builddep python

[ -e ${PYTHON_BASE_DIR}] || mkdir -p $PYTHON_BASE_DIR
[ -e ${TEMP_DIR}] || mkdir -p $TEMP_DIR

wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz
tar xvf Python-${PYTHON_VERSION}* && rm -rf Python-${PYTHON_VERSION}.tgz
mv ./Python-${PYTHON_VERSION} $TEMP_DIR
chown -R $USER_NAME:$USER_NAME $USER_HOME_DIR/*
cd $TEMP_DIR/Python-${PYTHON_VERSION}
./configure --prefix=${PYTHON_BASE_DIR} --enable-shared --enable-ipv6 LDFLAGS=-Wl,-rpath=${PYTHON_BASE_DIR}/lib,--disable-new-dtags
make
sudo make install

cd $USER_HOME_DIR
rm -rf $TEMP_DIR/*
echo -e "\nPATH=$PATH:$PYTHON_BASE_DIR/bin" >> $USER_HOME_DIR/.bashrc
chown -R $USER_NAME:$USER_NAME $PYTHON_BASE_DIR