Vagrant project provides all needed installed requirements for remote development with VSCode and Remote SSH plugin

~/.ssh/config Example (needed for VSCode)
```bash
Host web-developer-host
    HostName 192.168.22.22
    Port 22
    User vagrant
    IdentityFile D:\path\to\private_key
```

For provisioning host:
```bash
vagrant up
```